import Vue from 'vue'
import poket from './poket.vue'
Vue.config.productionTip = false

new Vue({
  render: function (h) { 
    return h(poket) 
  },
}).$mount('#app')



